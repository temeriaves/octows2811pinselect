## OctoWS2811PinSelect

A version of Paul Stoffregen's OctoWS2811 library modified to allow disabling unwanted led strip output channels so that they do not interfere with their assigned pins. By default, every output is disabled and must be specifically enabled with the "enableChannel" method. Otherwise the usage is exactly the same. This has not been tested much yet so be warned.

"enableChannel()" uses channel numbers. They map to the Teensy pins like this:

```
pin 2 -> channel 0 strip #1
pin 14 -> channel 1 strip #2
pin 7 -> channel 2 strip #3
pin 8 -> channel 3 strip #4
pin 6 -> channel 4 strip #5
pin 20 -> channel 5 strip #6
pin 21 -> channel 6 strip #7
pin 5 -> channel 7 strip #8
```
